""" Module with functionalities for comparison of attribute values as well as
    record pairs. The record pair comparison function will return a dictionary
    of the compared pairs to be used for classification.
"""
import collections
import time

import numpy as np

Q = 2  # Value length of q-grams for Jaccard and Dice comparison function

use_cache = False
q_cash = {}

runtime_cache = {}


# extract q grams
def q_grams(input_value: str, q: int = Q, index: int = 1, output_set=None) -> set:
    # end if all input has been processed
    if index > len(input_value) + q - 1:
        return output_set

    if output_set is None:
        output_set = set()

    # cover cases on the edge of the input string to fill them up with special characters
    value = (q - index) * "#" + input_value[max(index - q, 0):index] + (index - len(input_value)) * "#"

    new_output_set = output_set.union({value})

    return q_grams(input_value, q, index + 1, new_output_set)


# extract q grams
def q_grams_v2(input_value: str, q: int = Q, index: int = 1, output: tuple = None) -> tuple:
    # end if all input has been processed
    if index > len(input_value) + q - 1:
        return output

    if output is None:
        output = tuple()

    # cover cases on the edge of the input string to fill them up with special characters
    value = (q - index) * "#" + input_value[max(index - q, 0):index] + (index - len(input_value)) * "#"

    new_output = (*output, value)

    return q_grams_v2(input_value, q, index + 1, new_output)


def get_from_cache(v: str) -> set:
    global q_cash
    # check for cache miss
    if v in q_cash:
        return q_cash[v]
    else:
        qs = q_grams(v)
        q_cash[v] = qs
        return qs


# convert input value into multi list
def bag(input_value: str) -> dict:
    return dict(collections.Counter(input_value))


def subtract_bag(b1: dict, b2: dict) -> int:
    b1_counts = collections.Counter(b1)
    b2_counts = collections.Counter(b2)
    b1_counts.subtract(b2_counts)

    return sum([x[1] for x in b1_counts.items() if x[1] > 0])


# =============================================================================
# First the basic functions to compare attribute values

def exact_comp(val1, val2):
    """Compare the two given attribute values exactly, return 1 if they are the
       same (but not both empty!) and 0 otherwise.
    """

    # If at least one of the values is empty return 0
    #
    if (len(val1) == 0) or (len(val2) == 0):
        return 0.0

    elif val1 != val2:
        return 0.0
    else:  # The values are the same
        return 1.0


# -----------------------------------------------------------------------------
# TODO 2.1 implement jaccard similarity
def jaccard_comp(val1, val2):
    """Calculate the Jaccard similarity between the two given attribute values
       by extracting sets of sub-strings (q-grams) of length q.

       Returns a value between 0.0 and 1.0.
    """

    # If at least one of the values is empty return 0
    #
    if (len(val1) == 0) or (len(val2) == 0):
        return 0.0

    # If both attribute values exactly match return 1
    #
    elif val1 == val2:
        return 1.0

    # ********* Implement Jaccard similarity function here *********

    def get_jaccard_sim(v1: str, v2: str) -> float:
        if use_cache:
            set1 = get_from_cache(v1)
            set2 = get_from_cache(v2)
        else:
            set1 = q_grams(v1)
            set2 = q_grams(v2)

        len_int = len(set1.intersection(set2))
        len_un = len(set1) + len(set2) - len_int

        return len_int / len_un

    # def get_jaccard_sim_v2(v1: str, v2: str) -> float:

    jacc_sim = get_jaccard_sim(val1, val2)

    # ************ End of your Jaccard code *************************************

    assert 0.0 <= jacc_sim <= 1.0

    return jacc_sim


# -----------------------------------------------------------------------------
# TODO 2.2 implement dice similarity
def dice_comp(val1, val2):
    """Calculate the Dice coefficient similarity between the two given attribute
       values by extracting sets of sub-strings (q-grams) of length q.

       Returns a value between 0.0 and 1.0.
    """

    # If at least one of the values is empty return 0
    #
    if (len(val1) == 0) or (len(val2) == 0):
        return 0.0

    # If both attribute values exactly match return 1
    #
    elif val1 == val2:
        return 1.0

    # ********* Implement Dice similarity function here *********

    def get_dice_sim(v1: str, v2: str):

        if use_cache:
            set1 = get_from_cache(v1)
            set2 = get_from_cache(v2)
        else:
            set1 = q_grams(v1)
            set2 = q_grams(v2)

        len_inter = len(set1.intersection(set2))
        len_add = len(set1) + len(set2)

        return 2 * len_inter / len_add

    dice_sim = get_dice_sim(val1, val2)

    # ************ End of your Dice code ****************************************

    assert 0.0 <= dice_sim <= 1.0

    return dice_sim


# -----------------------------------------------------------------------------

JARO_MARKER_CHAR = chr(1)  # Special character used in the Jaro, Winkler comp.


def jaro_comp(val1, val2):
    """Calculate the similarity between the two given attribute values based on
       the Jaro comparison function.

       As described in 'An Application of the Fellegi-Sunter Model of Record
       Linkage to the 1990 U.S. Decennial Census' by William E. Winkler and Yves
       Thibaudeau.

       Returns a value between 0.0 and 1.0.
    """

    # If at least one of the values is empty return 0
    #
    if (val1 == '') or (val2 == ''):
        return 0.0

    # If both attribute values exactly match return 1
    #
    elif val1 == val2:
        return 1.0

    len1 = len(val1)  # Number of characters in val1
    len2 = len(val2)  # Number of characters in val2

    halflen = int(max(len1, len2) / 2) - 1

    assingment1 = ''  # Characters assigned in val1
    assignment2 = ''  # Characters assigned in val2

    workstr1 = val1  # Copy of original value1
    workstr2 = val2  # Copy of original value1

    common1 = 0  # Number of common characters
    common2 = 0  # Number of common characters

    for i in range(len1):  # Analyse the first string
        start = max(0, i - halflen)
        end = min(i + halflen + 1, len2)
        index = workstr2.find(val1[i], start, end)
        if index > -1:  # Found common character, count and mark it as assigned
            common1 += 1
            assingment1 = assingment1 + val1[i]
            workstr2 = workstr2[:index] + JARO_MARKER_CHAR + workstr2[index + 1:]

    for i in range(len2):  # Analyse the second string
        start = max(0, i - halflen)
        end = min(i + halflen + 1, len1)
        index = workstr1.find(val2[i], start, end)
        if index > -1:  # Found common character, count and mark it as assigned
            common2 += 1
            assignment2 = assignment2 + val2[i]
            workstr1 = workstr1[:index] + JARO_MARKER_CHAR + workstr1[index + 1:]

    if common1 != common2:
        common1 = float(common1 + common2) / 2.0

    if common1 == 0:  # No common characters within half length of strings
        return 0.0

    transposition = 0  # Calculate number of transpositions

    for i in range(len(assingment1)):
        if assingment1[i] != assignment2[i]:
            transposition += 1
    transposition = transposition / 2.0

    common1 = float(common1)

    jaro_sim = 1. / 3. * (common1 / float(len1) + common1 / float(len2) +
                          (common1 - transposition) / common1)

    assert (jaro_sim >= 0.0) and (jaro_sim <= 1.0), \
        'Similarity weight outside 0-1: %f' % jaro_sim

    return jaro_sim


# -----------------------------------------------------------------------------
# TODO 3.1 implement the extended version of Jaro Winkler
def jaro_winkler_comp(val1, val2):
    """Calculate the similarity between the two given attribute values based on
       the Jaro-Winkler modifications.

       Applies the Winkler modification if the beginning of the two strings is
       the same.

       As described in 'An Application of the Fellegi-Sunter Model of Record
       Linkage to the 1990 U.S. Decennial Census' by William E. Winkler and Yves
       Thibaudeau.

       If the beginning of the two strings (up to first four characters) are the
       same, the similarity weight will be increased.

       Returns a value between 0.0 and 1.0.
    """

    # If at least one of the values is empty return 0
    #
    if (val1 == '') or (val2 == ''):
        return 0.0

    # If both attribute values exactly match return 1
    #
    elif val1 == val2:
        return 1.0

    # First calculate the basic Jaro similarity
    #
    jaro_sim = jaro_comp(val1, val2)
    if jaro_sim == 0:
        return 0.0  # No common characters

    # ********* Implement Winkler similarity function here *********

    def get_jaro_winkler(v1: str, v2: str, jaro: float) -> float:

        def max_prefix_length(v1: str, v2: str) -> int:
            def _get_prefix(v1: str, v2: str, prefix: str = "") -> str:
                if len(v1) == 0 or len(v2) == 0:
                    return prefix

                if v1[0] != v2[0]:
                    return prefix

                return _get_prefix(v1[1:], v2[1:], prefix + v1[0])

            return len(_get_prefix(v1, v2))

        return jaro + (0.1 * min(4, max_prefix_length(v1, v2))) * (1 - jaro)

    jw_sim = get_jaro_winkler(val1, val2, jaro_sim)

    # ************ End of your Winkler code *************************************

    assert (jw_sim >= jaro_sim), 'Winkler modification is negative'
    assert (jw_sim >= 0.0) and (jw_sim <= 1.0), \
        'Similarity weight outside 0-1: %f' % jw_sim

    return jw_sim


# -----------------------------------------------------------------------------
# TODO 2.3 implement bag distance based similarity
def bag_dist_sim_comp(val1, val2):
    """Calculate the bag distance similarity between the two given attribute
       values.

       Returns a value between 0.0 and 1.0.
    """

    # If at least one of the values is empty return 0
    #
    if (len(val1) == 0) or (len(val2) == 0):
        return 0.0

    # If both attribute values exactly match return 1
    #
    elif val1 == val2:
        return 1.0

    # ********* Implement bag similarity function here *********
    # Extra task only

    def get_bag_sim(v1: str, v2: str) -> float:
        bag1 = bag(v1)
        bag2 = bag(v2)

        b1_m_b2 = subtract_bag(bag1, bag2)
        b2_m_b1 = subtract_bag(bag2, bag1)

        return 1 - max(b1_m_b2, b2_m_b1) / max(len(v1), len(v2))

    bag_sim = get_bag_sim(val1, val2)

    # ************ End of your bag distance code ********************************

    assert 0.0 <= bag_sim <= 1.0

    return bag_sim


# -----------------------------------------------------------------------------
# TODO 3.2 implement Edit distance based comparison
def edit_dist_sim_comp(val1, val2):
    """Calculate the edit distance similarity between the two given attribute
       values.

       Returns a value between 0.0 and 1.0.
    """

    # If at least one of the values is empty return 0
    #
    if (len(val1) == 0) or (len(val2) == 0):
        return 0.0

    # If both attribute values exactly match return 1
    #
    elif val1 == val2:
        return 1.0

    # ********* Implement edit distance similarity here *********

    def get_edit_value(v1: str, v2: str, matrix: np.ndarray, i: int, j: int) -> int:

        # cover cases, where either i or j are 0
        if j == 0:
            return i
        elif i == 0:
            return j

        # calculate distances
        d1 = matrix[i, j - 1] + 1
        d2 = matrix[i - 1, j] + 1
        d3 = matrix[i - 1, j - 1] + (0 if v1[i] == v2[j] else 1)

        # return minimum
        return min(d1, d2, d3)

    def get_edit_matrix(v1: str, v2: str) -> np.ndarray:

        # initialize matrix, add one to each dimension for comparison with empty character
        matrix = np.zeros((len(v1) + 1, len(v2) + 1), dtype=int)

        # add empty comparison character
        processed_v1 = "#" + v1
        processed_v2 = "#" + v2

        # calculate edit values for each field
        for i in range(matrix.shape[0]):
            for j in range(matrix.shape[1]):
                matrix[i, j] = get_edit_value(processed_v1, processed_v2, matrix, i, j)

        return matrix

    def get_levenshtein_sim(v1: str, v2: str) -> float:

        edit_matrix = get_edit_matrix(v1, v2)

        edit_distance = edit_matrix[len(v1), len(v2)]

        return 1 - (edit_distance / max(len(v1), len(v2)))

    edit_sim = get_levenshtein_sim(val1, val2)

    # ************ End of your edit distance code *******************************

    assert 0.0 <= edit_sim <= 1.0

    return edit_sim


# -----------------------------------------------------------------------------

# Additional comparison functions for: (extra tasks for students to implement)
# - dates
# - ages
# - phone numbers
# - emails
# etc.

# =============================================================================
# Function to compare a block

def compareBlocks(blockA_dict, blockB_dict, recA_dict, recB_dict, \
                  attr_comp_list):
    """Build a similarity dictionary with pair of records from the two given
       block dictionaries. Candidate pairs are generated by pairing each record
       in a given block from data set A with all the records in the same block
       from dataset B.

       For each candidate pair a similarity vector is computed by comparing
       attribute values with the specified comparison method.

       Parameter Description:
         blockA_dict    : Dictionary of blocks from dataset A
         blockB_dict    : Dictionary of blocks from dataset B
         recA_dict      : Dictionary of records from dataset A
         recB_dict      : Dictionary of records from dataset B
         attr_comp_list : List of comparison methods for comparing individual
                          attribute values. This needs to be a list of tuples
                          where each tuple contains: (comparison function,
                          attribute number in record A, attribute number in
                          record B).

       This method returns a similarity vector with one similarity value per
       compared record pair.

       Example: sim_vec_dict = {(recA1,recB1) = [1.0,0.0,0.5, ...],
                                (recA1,recB5) = [0.9,0.4,1.0, ...],
                                 ...
                               }
    """

    print('Compare %d blocks from dataset A with %d blocks from dataset B' % \
          (len(blockA_dict), len(blockB_dict)))

    sim_vec_dict = {}  # A dictionary where keys are record pairs and values
    # lists of similarity values

    # Iterate through each block in block dictionary from dataset A
    #
    comparisons = []
    for (block_bkv, rec_idA_list) in blockA_dict.items():

        # Check if the same blocking key occurs also for dataset B
        #
        if (block_bkv in blockB_dict):

            # If so get the record identifier list from dataset B
            #
            rec_idB_list = blockB_dict[block_bkv]

            # Compare each record in rec_id_listA with each record from rec_id_listB
            #
            for rec_idA in rec_idA_list:

                recA = recA_dict[rec_idA]  # Get the actual record A

                for rec_idB in rec_idB_list:
                    recB = recB_dict[rec_idB]  # Get the actual record B

                    # generate the similarity vector
                    #
                    sim_vec = compareRecord(recA, recB, attr_comp_list)

                    # Add the similarity vector of the compared pair to the similarity
                    # vector dictionary
                    #
                    sim_vec_dict[(rec_idA, rec_idB)] = sim_vec

    print('  Compared %d record pairs' % (len(sim_vec_dict)))
    print('')
    print(runtime_cache)
    print(f"Total comparison time: {sum(runtime_cache.values()):.3f}")

    return sim_vec_dict


# -----------------------------------------------------------------------------

def compareRecord(recA, recB, attr_comp_list):
    # global runtime_cache
    """Generate the similarity vector for the given record pair by comparing
       attribute values according to the comparison function and attribute
       numbers in the given attribute comparison list.

       Parameter Description:
         recA           : List of first record values for comparison
         recB           : List of second record values for comparison
         attr_comp_list : List of comparison methods for comparing attributes,
                          this needs to be a list of tuples where each tuple
                          contains: (comparison function, attribute number in
                          record A, attribute number in record B).

       This method returns a similarity vector with one value for each compared
       attribute.
    """

    sim_vec = []

    # Calculate a similarity for each attribute to be compared
    #
    for (comp_funct, attr_numA, attr_numB) in attr_comp_list:

        if attr_numA >= len(recA):  # Check there is a value for this attribute
            valA = ''
        else:
            valA = recA[attr_numA]

        if attr_numB >= len(recB):
            valB = ''
        else:
            valB = recB[attr_numB]

        valA = valA.lower()
        valB = valB.lower()

        start_time = time.time()
        sim = comp_funct(valA, valB)
        elapsed_time = time.time() - start_time
        # runtime_cache[comp_funct.__name__] = runtime_cache.setdefault(comp_funct.__name__, 0) + elapsed_time
        sim_vec.append(sim)

    return sim_vec

# -----------------------------------------------------------------------------

# End of program.
